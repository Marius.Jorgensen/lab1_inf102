package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        Collections.sort(list); // Sorterer listen
        
        int count = 1; // Starteren teller som kontrollerer repeterende tall 
        T prevElement = null; // Sjekker elementet før
        
        // Itererer gjennom den sorterte listen
        for (T element : list) {
            // Hvis  elementet er det samme som den forrige
            if (element.equals(prevElement)) {
                count++; // Øker count med 1 hvis tall repeteres 
                
                // Hvis telleren kommer opp i 3 har vi en triplikat  
                if (count >= 3) {
                    return element; // Returnerer triplikatet
                }
            } else {
                // Hvis elementet ikke er det samme som den forrige
                prevElement = element; // Setter forrige element til elementet
                count = 1; // Setter telleren til 1
            }
        }

        // Hvis det ikke finnes en triplikat returneres null
        return null;
    }
}

    

